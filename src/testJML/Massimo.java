package testJML;

public class Massimo {
	 //@ invariant MIN == 0;
	public final static int MIN = 0;
	/*@ public behavior
	 requires vett!=null;

	 @*/
	public static void stampaVettore (int[] vett) {
	for (int i = 0; i < vett.length; i++)
	System.out.print(i + ":" + vett[i] + ", ");
	 System.out.println();
	 }
	/*@ public behavior
	 requires vett!=null && (\forall int i; 0 <= i && i < vett.length; vett[i] > 0);
	 ensures (\exists int i; 0 <= i && i < vett.length; vett[i] == \result) &&
	 (\forall int j; 0 <= j && j < vett.length; \result >= vett[j]) ;

	 @*/
	public static int massimo(int[] vett) {

	int result = MIN;
	int i = 0;
	while(i < vett.length) {
	 if (vett[i] > result)
	 result = vett[i];
	 i++;
	 }
	 return result;
	 }

	public static void main(String[] args) {
	int[] a = { 1, 5, 3 };
	 stampaVettore(a);
	System.out.println("massimo: " + massimo(a) +
	 "\n****");
	 }
}