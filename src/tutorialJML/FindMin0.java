package tutorialJML;

public class FindMin0 {
	static int min(int a, int b){
		int result= a<b?a:b;
		//@assert result <= a && result <=b;
		return result;
	}
	//@ requires array.length>0;
	static int findMinInArray(int[] array){
		int smallest = array[0];
		for(int num:array){
			int newSmallest=min(num, smallest);
			//@assert newSmallest<=num && newSmallest<=smallest;
			smallest=newSmallest;
		}
		return smallest;
	}

}
