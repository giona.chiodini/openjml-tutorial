package tutorialJML;

public class FindMin2 {
	
	
	
	//@ensures \result <= a && \result <=b;
	static int min(int a, int b){
		int result= a<b?a:b;
		
		return result;
	}
	//@ requires array.length>0;
	//@ensures \forall int i; 0<=i<array.length; \result<=array[i];
	static int findMinInArray(int[] array){
		int smallest = array[0];
		for(int num:array){
			int newSmallest=min(num, smallest);
			//@assert newSmallest<=num && newSmallest<=smallest;
			smallest=newSmallest;
		}
		return smallest;
	}

}

/*OpenJML non riesce a provare la postcondizione alla riga 16. Perch�? 
Bisogna tener conto che gli SMT solvers hanno una forte limitazione:
sono in grado di verificare se il teorema � corretto, ma non possono dire 
se il teorema non lo �. Infatti questa � la situazione della quale avevamo parlato prima:
un SMT solver non � in grado di dimostrare la correttezza del programma, anche se ci possiamo rendere
facilmente conto che il programma � ok.
In questi casi ci si deve chiedere perch� OpenJML non trova prove per il codice: � perch� le specifiche non sono sufficientemente 
dettagliate o perch� il codice � errato?
*/