package tutorialJML;

/**
 * Let openjml find errors and see if you can fix them.
 **/
public class JMLTestBuggy {

   /*@ public invariant x >= 0; @*/
   public int x;

   
   public JMLTestBuggy() {
     x = 1;
   }

   public void foo(){
     x = -1;
   }

   /*@ public normal_behaviour
     @ ensures x == \old(x) + 1;
     @*/
   public void inc() {
     x = x + 1;
   }
}