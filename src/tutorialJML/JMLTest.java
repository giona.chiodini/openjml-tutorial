package tutorialJML;


/**
 * Let openjml find errors and see if you can fix them.
 **/
public class JMLTest {

   /*@public invariant x >= 0; @*/
   public int x;

   public JMLTest() {
     x = 1;
   }

   public void foo(){
     x = 0;
   }

   /*@public normal_behaviour
    @requires x<Integer.MAX_VALUE;
     @ensures x == \old(x) + 1;
     @*/
   public void inc() {
     x = x + 1;
   }
}