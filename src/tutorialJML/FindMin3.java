package tutorialJML;

/*Proviamo quindi ad aiutare il codice, in questo caso il motivo per il 
  quale non riesce a dimostrarne la correttezza � la presenza di un ciclo senza loop invariant.
  Rimpiazziamo il loop scritto in precedenza con un loop con indice e scriviamo due loop_invariant.
 * */

public class FindMin3 {
	
	
	
	//@ensures \result <= a && \result <=b;
	static int min(int a, int b){
		int result= a<b?a:b;
		
		return result;
	}
	//@ requires array.length>0;
	//@ensures \forall int i; 0<=i<array.length; \result<=array[i];
	static int findMinInArray(int[] array){
		int smallest = array[0];
		
		//@loop_invariant 0<=i && i<= array.length;
		//@loop_invariant \forall int j; 0<=j<i; smallest <=array[j];
		for(int i=0; i<array.length; i++){
			int newSmallest=min(array[i], smallest);
			//@assert newSmallest<=array[i]&& newSmallest<=smallest;
			smallest=newSmallest;
		}
		return smallest;
	}

}
