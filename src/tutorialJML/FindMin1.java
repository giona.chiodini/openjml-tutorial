package tutorialJML;

public class FindMin1 {
	
	/*Spostiamo l'asserzione che avevo messo dentro fuori, come 
	postcondizione. In questo modo in fase di verifica alla riga 10
	si pu� contare sul fatto che vale il contratto e di conseguenza
	la asserzione � dimostrata.*/ 
	
	//@ensures \result <= a && \result <=b;
	static int min(int a, int b){
		int result= a>b?a:b;
		
		return result;
	}
	//@ requires array.length>0;
	static int findMinInArray(int[] array){
		int smallest = array[0];
		for(int num:array){
			int newSmallest=min(num, smallest);
			//@assert newSmallest<=num && newSmallest<=smallest;
			smallest=newSmallest;
		}
		return smallest;
	}

}
