package tutorialJML;

/*Proviamo adesso a trasformare la funzione di prima in una funzione ricorsiva
  */

public class FindMin4 {
	
	
	
	//@ensures \result <= a && \result <=b;
	static int min(int a, int b){
		int result= a<b?a:b;
		
		return result;
	}
	//@requires array.length>0;
	//@ensures \forall int j; 0<=j<array.length; \result<=array[j];
	//@requires 0<=i && i< array.length;
	//@requires i<array.length && \forall int j; 0<=j<i; smallest<=array[j];
	static int findMinInArray(int[] array, int smallest, int i){
				
		smallest= min(array[i], smallest);
		if(i+1<array.length){
			return findMinInArray(array, smallest, i+1);
		}
		else{
			return smallest;
		}
	}

}
